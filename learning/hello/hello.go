package main

import (
	"fmt"

	"bitbucket.org/theandruu/go-public/learning/stringutil"
)

func main() {
	fmt.Println(stringutil.HyphenateSpaces("The lazy fox jumped over the brown dog."))
}
