// Package stringutil contains utility functions for working with strings.
package stringutil

import "strings"

// HyphenateSpaces replaces all occurances of a space " " with a hyphen "-"
func HyphenateSpaces(s string) string {
	return strings.Replace(s, " ", "-", -1)
}
