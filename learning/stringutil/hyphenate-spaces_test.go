package stringutil

import "testing"

func TestHyphenateSpaces(t *testing.T) {
	cases := []struct {
		in, want string
	}{
		{"Hello world", "Hello-world"},
		{"Hello 世界", "Hello-世界"},
		{"", ""},
	}
	for _, c := range cases {
		got := HyphenateSpaces(c.in)
		if got != c.want {
			t.Errorf("HyphenateSpaces(%q) == %q, want %q", c.in, got, c.want)
		}
	}
}
