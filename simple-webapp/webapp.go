package main

import (
	"fmt"
	"html/template"
	"net/http"
	"regexp"
)

var templates = template.Must(template.ParseFiles("templates/theme.html"))
var validPath = regexp.MustCompile("^/(hello)/([a-zA-Z0-9 ]+)$")

// A Message to print to our screen
type Message struct {
	Name string
}

func viewHandler(w http.ResponseWriter, r *http.Request) {
	splitPath := validPath.FindStringSubmatch(r.URL.Path)
	if splitPath == nil {
		// Only handle pages we expect based on our 'validPath' variable
		fmt.Printf("Unable to find name param in %s\n", r.URL.Path)
		http.NotFound(w, r)
		return
	}

	// Name param is the 3rd element in the splice (index 2)
	message := Message{Name: splitPath[2]}

	// Pass a pointer to our message object
	templates.ExecuteTemplate(w, "theme.html", &message)
}

func main() {
	http.HandleFunc("/hello/", viewHandler)
	http.ListenAndServe(":8080", nil)
}
